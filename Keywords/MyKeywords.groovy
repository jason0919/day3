import com.kms.katalon.core.annotation.Keyword

public class MyKeywords {

	@Keyword
	def helloWorld(){
		println "Hello World!"
	}

	@Keyword
	def helloPerson(String name){
		println "Hello " + name + "!"
	}
}
